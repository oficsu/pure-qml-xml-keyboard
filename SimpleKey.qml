import QtQuick 2.0
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

Rectangle {
    id: keyRect

    property alias text: label.text
    property string icon: ""
    property color textColor: "white"
    property real textSize: 14

    Label {
        id: label
        visible: !icon

        font.pixelSize: textSize

        color: textColor
        anchors.fill: parent

        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter

    }

    Image {
        id: img
        visible: icon

        height: textSize
        width: textSize

        source: icon ? "keyboarIcons/" + icon + ".png" : ""

        anchors.centerIn: parent.Center
    }


    ColorOverlay{
        visible: icon
        anchors.fill: img
        source: img
        color: textColor
        antialiasing: true
    }
}
