pragma Singleton
import QtQuick 2.9

QtObject {
        id: keyAction

        readonly property int nothing: 0
        readonly property int clutched: 1
        readonly property int repeated: 2
}
