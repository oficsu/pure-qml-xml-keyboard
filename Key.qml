import QtQuick 2.0
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.XmlListModel 2.0
import "."

Item {
    id: key
    property color backgroundColor: "transparent"
    // когда кнопка нажата
    property color clickedColor: "#292E33"
    // ближайшая из кнопок к курсору при долгом нажатии
    property color clutchedColor: Material.accentColor
    property color textColor: "white"

    property real textSize: 14*dp

    property real widthFactor: 1
    width: width * widthFactor
    property real buttonRadius: dp*4

    property string icon: ""

    property int action: KeyTypes.clutched

    property bool shifted: false
    property string primaryKey: ""
    property string shiftKey: ""

    property string currentAdditionalKey: ""

    property string additionalKeySource: ""
    property string additionalKeyQuery: ""
    property var additionalKey: []

    property int additionalKeyCount: 0
    property int horizontalKeyCount: 1
    property int verticalKeyCount: 1

    property string currentKey: state == "Clutched" ? currentAdditionalKey
                                                    : shifted ? shiftKey
                                                              : primaryKey


    signal clicked(string k)
    property bool isPressed: false
    property bool isClutched: false


    function distanceToObject(item) {

        var itemCoords = grid.mapToGlobal(item.x, item.y)
        var mouseAreaCoordinates = keyArea.mapToGlobal(keyArea.mouseX, keyArea.mouseY)

        var itemX = itemCoords.x + key.width/2
        var itemY = itemCoords.y + key.height/2

        var areaX = mouseAreaCoordinates.x
        var areaY = mouseAreaCoordinates.y

        return /*distance*/ Math.sqrt(Math.pow(areaX - itemX, 2)
                                     +Math.pow(areaY - itemY, 2))
    }
    function distancesList() {
        var distancesAndCoordsList = [];

        for (var i = 0; i < keyGrid.count; i++)
        {
                var distancesAndCoords = {}
                distancesAndCoords.distance = distanceToObject(keyGrid.itemAt(i))
                distancesAndCoords.id = i
                distancesAndCoordsList.push(distancesAndCoords)
        }
        return distancesAndCoordsList
    }
    function nearest() {
        var distances = distancesList()

        // JS-магия для нахождения наименьшего расстояния
        var min = Math.min.apply(Math, distances.map(function(o) { return o.distance; }))

        // JS-магия для нахождения объекта с найденным расстоянием
        var distancesAndCoords = distances.find(function(o){ return o.distance === min; })

        return distancesAndCoords
    }

    SimpleKey {
        id: keyRect
        color: backgroundColor
        radius: buttonRadius
        anchors.fill: parent
        icon: key.icon
        text: shifted ? shiftKey
                      : primaryKey
    }

    Item {
        id: xxxx
        anchors.bottom: keyRect.bottom
        height: key.height * 1.5 + key.height * verticalKeyCount
        width: key.width
        opacity: 0


        Rectangle {
            id: clutchRect
            radius: buttonRadius + 1
            opacity: 0

            width: key.width
            height: parent.height
            color: clickedColor

            anchors.bottom: parent.bottom
            anchors.verticalCenterOffset: buttonRadius
        }

        Rectangle {
            id: additionalKeyBox
            width: key.width * horizontalKeyCount
            height: key.height * verticalKeyCount


            color: clickedColor

            GridLayout {
                id: grid
                rows: verticalKeyCount
                columns: horizontalKeyCount
                rowSpacing: 0
                columnSpacing: 0


                property var near: null

                property var prevNear: null

                onNearChanged: {
                    if (!near) return

                    if (prevNear) {
                        prevNear.isNear = false
                    }

                    // по координатам ближайшего делегата, находим его
                    var nearDelegate = keyGrid.itemAt(near.id)


                                                // берем его Rectangle
                    prevNear = nearDelegate ? nearDelegate : null


                    if (nearDelegate)
                        nearDelegate.isNear = true
                }


                Repeater {
                    id: keyGrid //
                    model: key.state == "Clutched" && additionalKeyCount ? mdl
                                                                         : shifted ? [shiftKey]
                                                                                   : [primaryKey]

                    property XmlListModel mdl: XmlListModel {
                        id: keysModel

                        source: additionalKeySource
                        query: additionalKeyQuery

                        XmlRole { name: "modelData"; query: "string()" }

                        onStatusChanged: {
                            if (status == XmlListModel.Ready) {
                                additionalKeyCount = count
                                console.log("set: ", count)
                            }
                        }

                    }

                    delegate: SimpleKey {
                        Layout.preferredHeight: key.height
                        Layout.preferredWidth: key.width
                        visible: index == 0 || key.state == "Clutched"

                        property bool isNear: false
                        onIsNearChanged: {
                            key.currentAdditionalKey = modelData
                        }
                        radius: key.buttonRadius
                        color: isNear && key.state == "Clutched" ? clutchedColor
                                                                 : "transparent"
                        textColor: key.textColor
                        icon: key.icon
                        text: key.state == "Clutched" ? modelData
                                                      : shifted ? shiftKey
                                                                : primaryKey
                    }
                }
            }




            anchors.horizontalCenter: parent.horizontalCenter

            anchors.top: clutchRect.top
            radius: buttonRadius + 1
        }

    }

    states: [
        State {
            name: "Clutched"
            when: isClutched && /*action == KeyTypes.clutched &&*/ additionalKeyCount
            PropertyChanges {
                target: key

                horizontalKeyCount: additionalKeyCount ? Math.ceil(Math.sqrt(additionalKeyCount))
                                                                      : 1
                verticalKeyCount: additionalKeyCount ? Math.round(Math.sqrt(additionalKeyCount))
                                                                    : 1
            }
            PropertyChanges {
                target: clutchRect
                opacity: 1
            }
            PropertyChanges {
                target: keyGrid
                //model: additionalKey.length ? additionalKey
                                         //   : [primaryKey]
            }
            PropertyChanges {
                target: xxxx
                opacity: 1
            }
            PropertyChanges {
                target: keyArea
                hoverEnabled: true
            }
            PropertyChanges {
                target: grid
                near: nearest()
            }
        },
        State {
            name: "pressed"
            when: isPressed
            PropertyChanges {
                target: key
            }
            PropertyChanges {
                target: clutchRect
                opacity: 1
            }
            PropertyChanges {
                target: xxxx
                opacity: 1
            }
        }

    ]


    Timer {
        id: timer
        interval: 600
        repeat: false
        running: isPressed
        onTriggered: {
            isClutched = true
        }
    }

    MouseArea {
        id: keyArea
        anchors.fill: parent

        onMouseXChanged: {
        }
        hoverEnabled: false

        onPressed: key.isPressed = true

        onReleased:  {
            key.isPressed = false
            key.clicked(currentKey)
            console.log(currentKey)
            key.isClutched = false


        }
    }
}
