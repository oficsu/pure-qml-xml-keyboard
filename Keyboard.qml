import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3
import QtQuick.XmlListModel 2.0

Item {
    id: keyboard

    property Item inputField: activeFocusItem ? (activeFocusItem.cursorVisible ? activeFocusItem : null) : null
    property bool activated: true
    property bool showSecondaryKeys: false
    property bool shiftActive: false
    property bool capslock: false

    property string layout: "layouts/rusLayout.xml"

    onInputFieldChanged: activated = inputField
    function insert(key) {
        inputField.insert(inputField.cursorPosition, key)
    }

    function remove() {
        inputField.remove(inputField.cursorPosition, inputField.cursorPosition - 1)
    }

    function moveRight() {
        inputField.cursorPosition -= 1
    }

    function moveLeft() {
        inputField.cursorPosition += 1
    }

    function hide() {
        activated = false
        forceActiveFocus()
    }
    enabled: activated

    anchors.left: parent.left
    anchors.right: parent.right
    anchors.bottom: parent.bottom

    function keySizeCalc() {

    }



    height: maxRows * keySize
    property var keysInRow: []
    property var rows: []

    property color keyboardColor: "#333333"

    property int maxKeyInRow: 1
    property real keySize: keyboard.width / maxKeyInRow - 1
    property int maxRows: null

    Rectangle {
        anchors.fill: parent
        color: keyboardColor
    }

    ColumnLayout {
        id: rowRepeater
        spacing: 0

        anchors.fill: parent


//        Row {
//            id: keyRow
//            spacing: 0

//            Layout.alignment: Qt.AlignHCenter


//            Key {
//                width: keySize
//                height:  keySize
//                primaryKey: "prim"
//                shiftKey  : "shift"

//                Component.onCompleted: {
//                    console.log("I'm created: ", primaryKey)
//                }
//            }
//        }

        Repeater {
            model: XmlListModel {
                id: rowsModel
                source: keyboard.layout
                query: "/layout/row"

                onStatusChanged: {
                    maxRows = count
                }
            }

            Row {
                id: keyRow
                spacing: 0

                Layout.alignment: Qt.AlignHCenter

                property int rowIndex: index


                Repeater {
                    model: XmlListModel {
                        id: keysModel

                        source: keyboard.layout
                        query: "/layout/row[" + (rowIndex + 1) + "]/key"

                        XmlRole { name: "prim"; query: "prim/string()" }
                        XmlRole { name: "shift"; query: "shift/string()" }
                        XmlRole { name: "ico"; query: "icon/string()" }
                        XmlRole { name: "wfactor"; query: "wfactor/string()" }

                        onStatusChanged: {
                            if (status == XmlListModel.Ready) {
                                keysInRow.push(count)

                                maxKeyInRow = Math.max.apply(Math, keysInRow)
                            }
                        }
                    }

                    Key {
                        width: keyboard.width / maxKeyInRow
                        height: keyboard.width / maxKeyInRow
                        primaryKey: prim
                        shiftKey  : shift
                        icon: ico
                        widthFactor: wfactor ? wfactor : 1
                        backgroundColor: "#333333"
                        additionalKeySource: keyboard.layout
                        additionalKeyQuery: "/layout/row[" + (rowIndex + 1) + "]/key[" + (keyIndex +1) + "]/addingKeys/adding"

                        property int keyIndex: index


                        onHeightChanged: {
                            console.log("I'm changed: ", prim)
                            console.log(height)
                        }

                        Component.onCompleted: {
                            console.log("I'm created: ", prim)
                            console.log(height)
                        }
                    }
                }

            }

        }




    }



}
