import QtQuick 2.0

ListModel {
    property string title: "Список документов"

    ListElement {
        subText: "0"
        topText: "Не выбрано"
    }
    ListElement {
        subText: "A"
        topText: "Мотоцикл"
    }
    ListElement {
        subText: "B"
        topText: "Легковой автомобиль"
    }
    ListElement {
        subText: "C"
        topText: "Гpузовой автомобиль"
    }
    ListElement {
        subText: "D"
        topText: "Автобус"
    }
    ListElement {
        subText: "E"
        topText: "Пpицеп"
    }
    ListElement {
        subText: "F"
        topText: "Трамвай"
    }
    ListElement {
        subText: "G"
        topText: "Троллейбус"
    }
    ListElement {
        subText: "H"
        topText: "Трактор"
    }
    ListElement {
        subText: "K"
        topText: "Самоходный механизм"
    }
    ListElement {
        subText: "T"
        topText: "Подвижной состав (железнодорожный)"
    }
    ListElement {
        subText: "J"
        topText: "Велосипед"
    }
    ListElement {
        subText: "L"
        topText: "Гужевой"
    }
    ListElement {
        subText: "М"
        topText: "Мопед, скутер"
    }
}
