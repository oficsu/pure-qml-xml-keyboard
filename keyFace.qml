import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

import QtQuick.Controls 2.2

Item {
    id: keyFace

    property alias color: rec.color
    property alias isNear: rec.isNear

    Rectangle {
        id:rec

        property bool isNear: false //grid.childAt(near.x, near.y).children[0] === rec

        radius: key.buttonRadius

        anchors.fill: parent
        color: isNear ? clutchedColor
                      : "transparent"
    }

    Label {
        color: textColor
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: modelData
    }

}
