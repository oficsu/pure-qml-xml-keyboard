import QtQuick 2.0

Item {
    property Item inputField: activeFocusItem ? (activeFocusItem.cursorVisible ? activeFocusItem : null) : null
    property bool activated: true
    property bool showSecondaryKeys: false
    property bool shiftActive: false
    property bool capslock: false

    onInputFieldChanged: activated = inputField
    function insert(key) {
        inputField.insert(inputField.cursorPosition, key)
    }

    function remove() {
        inputField.remove(inputField.cursorPosition, inputField.cursorPosition - 1)
    }

    function moveRight() {
        inputField.cursorPosition -= 1
    }

    function moveLeft() {
        inputField.cursorPosition += 1
    }

    function hide() {
        activated = false
        forceActiveFocus()
    }
    enabled: activated

    MouseArea {
        id: keyArea
        anchors.fill: parent
        Component.onCompleted: {
            console.log("height", height)
        }
        onHoveredChanged: {
            console.log("dsfsf", height)
        }




        onPressed: {
            key.isPressed = true
            console.log("clicked!")
        }

        enabled: true

        hoverEnabled: true
        Rectangle {
            anchors.fill: parent
            color: "white"
        }

        onReleased:  {
            key.isPressed = false

            var touchCoordinate = Qt.point(touchPoints[0].x, touchPoints[0].y)
            if (keyArea.contains(touchCoordinate)) {
                key.clicked()

            }
        }
    }
}
